#include <iostream>
#include <cstdlib>
#include <vector>
#include "../hechizo/hechizo.h"
#include "../mago/mago.h"
#include "../set/set.h"
#include "../map/map.h"
#include "../maxheap/maxheap.h"
#include "escuela_magia.h"
#include "../common/Common.h"
#include "../common/Test.h"

using namespace std;

/// Proposito: Retorna todos los hechizos aprendidos por los magos.
/// Eficiencia: ?
Set hechizosAprendidos(EscuelaDeMagia m) {
   Set s = emptyS();
   vector<string> nombres = magos(m);
   for(int i = 0 ; i < nombres.size() ; i++){
        s = unionS(s, hechizosDe(nombres[i],m) );
   }
   return s;
}

/// Proposito: Indica si existe un mago que sabe todos los hechizos enseñados por la escuela.
/// Eficiencia: ?
bool hayUnExperto(EscuelaDeMagia m) {
   return (!estaVacia(m)) && ((leFaltanAprender(nombreMago(unEgresado(m)),m)) == 0) ;
}

/// Proposito: Devuelve una maxheap con los magos que saben todos los hechizos dados por la escuela, quitándolos de la escuela.
/// Eficiencia: ?
MaxHeap egresarExpertos(EscuelaDeMagia m) {
   MaxHeap h = emptyH();

   while(hayUnExperto(m) ){
        insertH(unEgresado(m) , h);
        quitarEgresado(m);
   }
   return h;
}

TEST(test_fundarEscuela_estaVacia, {
   EscuelaDeMagiaSt* m = fundarEscuela();
	ASSERT_EQ(estaVacia(m), true);
})

TEST(test_regitrar_unMago_ConDosHechizos, {
   EscuelaDeMagiaSt* m = fundarEscuela();
   registrar("Jere" , m);
   enseniar(crearHechizo("piro" , 90) , "Jere" , m);
    enseniar(crearHechizo("hielo" , 90) , "Jere" , m);
	ASSERT_EQ(sizeS(hechizosDe("Jere" , m)), 2);
})

TEST(test_regitrar_dosMagos_ConUnHechizoDiferente_FaltaAprenderUnHechizo, {
   EscuelaDeMagiaSt* m = fundarEscuela();
   registrar("Jere" , m);
   enseniar(crearHechizo("piro" , 90) , "Jere" , m);
   registrar("Fede" , m);
   enseniar(crearHechizo("hielo" , 100) , "Fede" , m);
	ASSERT_EQ(leFaltanAprender("Fede",m) , 1);
})

TEST(test_regitrar_dosMagos_ConMismoNombre_SoloExiste_ElPrimero, {
   EscuelaDeMagiaSt* m = fundarEscuela();
   registrar("Jere" , m);
   enseniar(crearHechizo("piro" , 90) , "Jere" , m);
   registrar("Jere" , m);
   enseniar(crearHechizo("hielo" , 100) , "Fede" , m);
	ASSERT_EQ(sizeS(hechizosAprendidos(m)), 2);
})

TEST(test_regitrar_unMagoAprendeTodosLosHechizosDeLaEscuela_HayUnExperto, {
   EscuelaDeMagia m = fundarEscuela();
   registrar("Jere" , m);
    registrar("RFR" , m);
   enseniar(crearHechizo("piro" , 90) , "Jere" , m);
   enseniar(crearHechizo("hielo" , 100) , "Jere" , m);
   enseniar(crearHechizo("rayo" , 70) , "Jere" , m);
   enseniar(crearHechizo("piro" , 90) , "RFR" , m);
   enseniar(crearHechizo("hielo" , 100) , "RFR" , m);
   enseniar(crearHechizo("rayo" , 70) , "RFR" , m);
    quitarEgresado(m);
	ASSERT_EQ(nombreMago(unEgresado(m)) , "RFR");
})

TEST(test_regitrar_dosMagosConHechizoDiferente_HechizosDeLaEscuelaSonDos, {
   EscuelaDeMagiaSt* m = fundarEscuela();
   registrar("Jere" , m);
   enseniar(crearHechizo("piro" , 90) , "Jere" , m);
   registrar("Fede" , m);
   enseniar(crearHechizo("hielo" , 100) , "Fede" , m);
	ASSERT_EQ(sizeS(hechizosAprendidos(m)), 2);
})

TEST(test_regitrar_variosMagosConhechizos_hechizosAprendidos6, {
   EscuelaDeMagiaSt* m = fundarEscuela();
   Hechizo piro = crearHechizo("piro" , 90);
   registrar("Jere" , m);
   enseniar(piro , "Jere" , m);
   registrar("Fede" , m);
   enseniar(crearHechizo("hielo" , 100) , "Fede" , m);
   enseniar(crearHechizo("rayo" , 100) , "Fede" , m);
   registrar("Camila" , m);
   registrar("Martin" , m);
   enseniar(crearHechizo("aqua" , 100) , "Camila" , m);
   enseniar(crearHechizo("gravedad" , 100) , "Camila" , m);
   enseniar( piro , "Camila" , m);
   enseniar(crearHechizo("invocacion" , 90) , "Jere" , m);
	ASSERT_EQ(sizeS(hechizosAprendidos(m)), 6);
})


TEST(test_regitrar_dosMagosunoConUnHechizoYOtroConDos_ElDeDosEsElEgresado, {
   EscuelaDeMagiaSt* m = fundarEscuela();
   registrar("Jere" , m);
   Hechizo piro = crearHechizo("piro" , 90);
   enseniar(piro, "Jere" , m);
   registrar("Fede" , m);
   enseniar(crearHechizo("hielo" , 100) , "Fede" , m);
   enseniar(piro, "Fede" , m);
	ASSERT_EQ(nombreMago(unEgresado(m)) ,"Fede" );
})

TEST(test_regitrar_dosMagos_conHehizoDiferente_NoHayExperto, {
   EscuelaDeMagiaSt* m = fundarEscuela();
   registrar("Jere" , m);
   Hechizo piro = crearHechizo("piro" , 90);
   enseniar(piro, "Jere" , m);
   registrar("Fede" , m);
   enseniar(crearHechizo("hielo" , 100) , "Fede" , m);
	ASSERT_EQ(hayUnExperto(m) ,false );
})

TEST(test_regitrar_unMago_HayUnExperto, {
   EscuelaDeMagiaSt* m = fundarEscuela();
   registrar("Jere" , m);
   Hechizo piro = crearHechizo("piro" , 90);
   enseniar(piro, "Jere" , m);
	ASSERT_EQ(hayUnExperto(m) ,true );
})

TEST(test_regitrar_unMago_seEgresa_LaEscuelaEstaVacia, {
   EscuelaDeMagiaSt* m = fundarEscuela();
   registrar("Jere" , m);
   Hechizo piro = crearHechizo("piro" , 90);
   enseniar(piro, "Jere" , m);
   quitarEgresado(m);
	ASSERT_EQ(estaVacia(m) ,true );
})

TEST(test_regitrar_DosMagos_SeEgresanYLaEscuelaEstaVacia, {
   EscuelaDeMagiaSt* m = fundarEscuela();
   registrar("Jere" , m);
   Hechizo piro = crearHechizo("piro" , 90);
   enseniar(piro, "Jere" , m);
   registrar("Fede" , m);
   enseniar(piro, "Fede" , m);
   egresarExpertos(m);
	ASSERT_EQ(estaVacia(m) , true);
})

TEST(test_regitrar_TresMagos_SeEgresaUno_ElEgresadoEnElQueSabeMas, {
   EscuelaDeMagiaSt* m = fundarEscuela();
   registrar("Jere" , m);
   registrar("Fede" , m);
   registrar("Martin" , m);
   Hechizo piro = crearHechizo("piro" , 90);
   Hechizo hielo = crearHechizo("hielo" , 80);
   Hechizo rayo = crearHechizo("rayo" , 100);
   enseniar(piro, "Jere" , m);
   enseniar(hielo, "Jere" , m);
   enseniar(rayo, "Jere" , m);
   enseniar(hielo, "Fede" , m);
   enseniar(rayo, "Martin" , m);
   enseniar(piro, "Fede" , m);
   egresarExpertos(m);
	ASSERT_EQ(nombreMago(unEgresado(m)), "Fede");
})

TEST(test_destruirEscuela, {
   EscuelaDeMagiaSt* m = fundarEscuela();
   registrar("Jere" , m);
   Hechizo piro = crearHechizo("piro" , 90);
   enseniar(piro, "Jere" , m);
   registrar("Fede" , m);
   enseniar(piro, "Fede" , m);
   destruirEscuela(m);
   ASSERT_EQ(estaVacia(m) , true);
})

int main(){

    test_fundarEscuela_estaVacia();
    test_regitrar_unMago_ConDosHechizos();
    test_regitrar_dosMagos_ConUnHechizoDiferente_FaltaAprenderUnHechizo();
    test_regitrar_variosMagosConhechizos_hechizosAprendidos6();
    test_regitrar_unMagoAprendeTodosLosHechizosDeLaEscuela_HayUnExperto();
    test_regitrar_dosMagosConHechizoDiferente_HechizosDeLaEscuelaSonDos();
    test_regitrar_dosMagosunoConUnHechizoYOtroConDos_ElDeDosEsElEgresado();
    test_regitrar_dosMagos_conHehizoDiferente_NoHayExperto();
    test_regitrar_unMago_HayUnExperto();
    test_regitrar_unMago_seEgresa_LaEscuelaEstaVacia();
    test_regitrar_DosMagos_SeEgresanYLaEscuelaEstaVacia();
    test_regitrar_TresMagos_SeEgresaUno_ElEgresadoEnElQueSabeMas();
    test_destruirEscuela();
    return 0;
}


