#include "escuela_magia.h"
#include "../common/Common.h"

/// INV. REP:
/// - magos y magosOrdenados tienen los mismos magos.
/// - La cantidad de magos en el map es la misma cantidad que la de la maxHeap.

struct EscuelaDeMagiaSt {
   Set hechizos;
   MaxHeap magosOrdenados;
   Map magos;
};

/// Prop�sito: Devuelve una escuela vac�a.
/// O(1)
EscuelaDeMagia fundarEscuela() {
   EscuelaDeMagia e = new EscuelaDeMagiaSt;
   e->hechizos = emptyS();
   e->magosOrdenados = emptyH();
   e->magos = emptyM();
   return e;
}

/// Prop�sito: Indica si la escuela est� vac�a.
/// O(1)
bool estaVacia(EscuelaDeMagia m) {
   return isEmptyH(m->magosOrdenados);
}

/// Prop�sito: Incorpora un mago a la escuela (si ya existe no hace nada).
/// O(log m)
void registrar(string nombre, EscuelaDeMagia m) {
   if(lookupM(nombre, m->magos) != NULL){

   }else{
       Mago mago = crearMago(nombre);
       assocM(nombre, mago, m->magos);
       insertH(mago, m->magosOrdenados);
    }
}

/// Prop�sito: Devuelve los nombres de los magos registrados en la escuela.
/// O(m)
vector<string> magos(EscuelaDeMagia m) {
   return domM(m->magos);
}

/// Prop�sito: Devuelve los hechizos que conoce un mago dado.
/// Precondici�n: Existe un mago con dicho nombre.
/// O(log m)
Set hechizosDe(string nombre, EscuelaDeMagia m) {
   return hechizosMago(lookupM(nombre, m->magos));
}

/// Prop�sito: Dado un mago, indica la cantidad de hechizos que la escuela ha dado y �l no sabe.
/// Precondici�n: Existe un mago con dicho nombre.
/// O(log m)
int leFaltanAprender(string nombre, EscuelaDeMagia m) {
   return (sizeS(m->hechizos)) - (sizeS(hechizosDe(nombre, m))) ;
}

/// Prop�sito: Devuelve el mago que m�s hechizos sabe.
/// Precondici�n: La escuela no est� vac�a.
/// O(1)
Mago unEgresado(EscuelaDeMagia m) {

   return (maxH(m->magosOrdenados));
}

/// Prop�sito: Devuelve la escuela sin el mago que m�s sabe.
/// Precondici�n: La escuela no est� vac�a.
/// O(log m)
void quitarEgresado(EscuelaDeMagia m) {
   Mago mg = unEgresado(m);
   deleteMax(m->magosOrdenados);
   deleteM(nombreMago(mg), m->magos);
}

/// Prop�sito: Ense�a un hechizo a un mago existente, y si el hechizo no existe en la escuela es incorporado a la misma.
/// O(m . log m + log h)
void enseniar(Hechizo h, string nombre, EscuelaDeMagia m) {
   addS(h,m->hechizos);
   Mago mago = lookupM(nombre, m->magos);
   aprenderHechizo(h,mago);
   assocM(nombre, mago, m->magos);
   MaxHeap hv = m->magosOrdenados;
   MaxHeap hn = emptyH();
   while( !isEmptyH(hv)){
       insertH(maxH(hv),hn);
       deleteMax(hv);
   }
   m->magosOrdenados = hn;
}

/// Prop�sito: Libera toda la memoria creada por la escuela (incluye magos, pero no hechizos).
void destruirEscuela(EscuelaDeMagia m) {
   while (! isEmptyH(m->magosOrdenados)){
        destroyMago(maxH(m->magosOrdenados));
        deleteMax(m->magosOrdenados);
   }
   destroyH(m->magosOrdenados);
   destroyM(m->magos);
   destroyS(m->hechizos);
   delete m;
}


