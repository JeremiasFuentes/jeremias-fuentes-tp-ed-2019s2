#include "set.h"
#include "../common/Common.h"

struct Node {
   Hechizo elem; // el elemento que este nodo almacena
   Node* next; // siguiente nodo de la cadena de punteros
};

struct SetSt {
   int size; // cantidad de elementos del conjunto
   Node* first; // puntero al primer elemento
};

/**
  Invariantes de representacion:
    - size es la cantidad de nodos
    - no hay nodos con hechizos repetidos
**/

/// Proposito: retorna un conjunto de hechizos vacio
/// Costo: O(1)
Set emptyS() {
   Set h = new SetSt;
   h->first = NULL;
   h->size = 0;
   return h;
}

/// Proposito: retorna la cantidad de hechizos
/// Costo: O(1)
int sizeS(Set s) {
   return (s->size);
}

/// Proposito: indica si el hechizo pertenece al conjunto
/// Costo: O(h), h = cantidad de hechizos
bool belongsS(Hechizo h, Set s) {
   Node* n = s->first;
   while(n != NULL && (not mismoHechizo(h, n->elem))){
        n = n->next;
   }
   return n != NULL;
}

/// Proposito: agrega un hechizo al conjunto
/// Costo: O(h), h = cantidad de hechizos
void addS(Hechizo h, Set s) {
    if (! belongsS(h, s)){
       Node* n = new Node;
       n->elem = h;
       n->next = s->first;
       s->first = n;
       s->size++;
    }else{}
}

/// Proposito: borra un hechizo del conjunto (si no existe no hace nada)
/// Costo: O(h), h = cantidad de hechizos
void removeS(Hechizo h, Set s) {
    if(belongsS(h,s)){
        if(mismoHechizo(h,s->first->elem)){
           s->first = s->first->next;
           s->size--;
        }else{
           Node* anterior = s->first;
           Node* actual = anterior->next;
           while (not mismoHechizo(h,actual->elem)){
                anterior = actual;
                actual = actual->next;
           }
           anterior = actual->next;
           s->size--;
        }
    }else{}
}


/// Proposito: borra toda la memoria consumida por el conjunto (pero no la de los hechizos)
/// Costo: O(n)
void destroyS(Set s) {
   Node* p = s->first;
   while(p != NULL){
        Node* sig = p->next;
        delete p;
        p = sig;
   }
   delete s;
}

/// Proposito: retorna un nuevo conjunto que es la union entre ambos (no modifica estos conjuntos)
/// Costo: O(h^2), h = cantidad de hechizos
Set unionS(Set s1, Set s2) {
   Set s = emptyS();
   Node* sigElem1 = s1->first;
   Node* sigElem2 = s2->first;
   while (sigElem1 !=NULL){
        addS(sigElem1->elem, s);
        sigElem1 = sigElem1->next;
   }
   while (sigElem2 !=NULL){
        addS(sigElem2->elem, s);
        sigElem2 = sigElem2->next;
   }
   return s;
}
